# chatbot_beasiswa_using_rasa

chatbot untuk sampel web beasiswa

## Requirement
```bash
- python versi 3.7.6
- rasa versi 2.1.2
- rasa SDK versi 2.8.1
```

## Run App
- buka command prompt
- pindah direktori yang dituju
- buat environment python
```bash
python -m venv venv
```
- aktifkan python environment
```bash
venv/Scripts/activate
```
- pip install semua requirement yang dibutuhkan
- jalankan program 
```bash
rasa run --enable-api --cors "*"
```
- double click file index.html

## Referensi
https://github.com/rasahq/rasa
